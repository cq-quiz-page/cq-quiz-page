// Create a function that will generate new question objects
function newQuestion(params) {
    var temp = {
        question: params[0],
        choices: params[1],
        correctAnswer: params[2]
    };
    return temp;
}

// Create the array allQuestions and generate all of the new questions
var allQuestions = [
    ["<p>Which of the following do you tend to notice more when you visit any new city or country?<p/>", ["Their street fashion - the way the locals dress (FD)", "The signage - billboards, traffic signs, etc (CD)", "Their buildings and architecture (IAD)", "The people, their behaviour and cultures (All)"], 1],
    ["<p>While eating out at a restaurant, which of the following are you most likely to notice, apart from the food?<p/>", ["The ambience, furniture & decor (IAD)", "The way the menu card has been designed (CD)", "The people, their behaviour and way  of interacting with each other (ALL)", "The way people are dressed, the staff’s uniform (FD)"], 3],
    ["<p>Which of the following do you regularly like to engage with?<p/>", ["Drawing, Sketching, Painting, Doodling (ALL)", "Digital Illustrations - Adobe Photoshop / Illustrator (CD)", "Making 3-d models (IAD)", "Enjoy creating usable articles from materials available at home (FD)"], 0],
    ["<p>If given a chance to attend a webinar, which of the following topics would you choose?<p/>", ["Patterns and motifs associated with various cultures. (FD)", "Logos and Advertising campaigns of various brands, and their history (CD)", "Monuments from India or the world, and their history. (IAD)", "Master Artists and their famous paintings. (ALL)"], 2],
    ["<p>Which of the following do you follow or are most likely to follow, on social media?<p/>", ["Architect or Interior Stylist (IAD)", "Fashion Blogger or Stylist (FD)", "Illustrator or Sketching Artist (CD)", "Photographer (All)"], 0],
    ["<p>Which of these is most likely to happen with you?<p/>", ["You engage with a digital app and think, “I could make it better” (CD)", "You look at someone's outfit and think, “I could tweak it” (FD)", "You walk into a hotel’s lobby or a store in a mall and think, “This needs fixing” (IAD)", "All of the above (All)"], 1],
    ["<p>When you watch a period drama film or read a story set in a bygone era, do you ever feel the urge to have been born in that time just so that:<p/>", ["You could wear or make those kind of clothes (FD)", "You could use their unique and exotic artifacts and furniture (IAD)", "You could give the story a different ending  (CD)", "You could meet the artists of that time (All)"], 3],
    ["<p>Which of the following would you like to indulge in / work on?<p/>", ["Work that allows you to create a unique identity for yourself (All)", "Take inspiration from something, to create new and original designs.(FD)", "Come up with out of the box solution to everyday problems.(CD)", "Technical plans, drawings, and models (IAD)"], 1],
    ["<p>To produce creative work you need to<p/>", ["Work on your own without much interaction with others (All)", "Work collaboratively with a lot of people (All)", "Direct and supervise people (All)", "I don’t know my preference, OR I can work in any situation (All)"], 1],
    ["<p>Which of the following statements is true for you?<p/>", ["You enjoy building things using tools.  (IAD)", "When you hear a story, you feel an urge to tell it to others in your own way and by using imagery.(CD)", "People recognize certain types of clothes or patterns as “your style”.(FD)", "You notice the pattern in people’s behaviours and like to understand why they behave the way they do.(All)"], 1],
    ["<p>When drawing a scene, which of the following are you most likely to focus on?<p/>", ["Story Telling (CD)", "People (FD)", "Details such as buildings, furniture, etc.(IAD)", "Nature including trees, sky, ponds, etc. (All)"], 1]
].map(newQuestion);

//Randomize the array
//const randomQuestions = Math.floor(Math.random() * allQuestions.length);

// Create and initialize the total (to 0), number (to 0), and totalQuestions (to the length of allQuestions) variables
var total = 0, number = 0, totalQuestions = allQuestions.length, answers = [];

$(document).ready(function () {

    function newQuestionAnswers() {
        $("#content").fadeOut(500, function () {
            $("#answers").empty();
            if (number < totalQuestions)
                //$("#questCount").text("Question: " + (number + 1) + " of 10");
                var query = allQuestions[number];
            $("#question").html(query.question);

            // make sure to put in the name parameter and make sure that it's the same as the div that groups
            // the radio buttons together, otherwise they can all be checked at the same time, you'll never have
            // just one answer. The use of the html <label> element was discovered here:
            // http://stackoverflow.com/questions/5795499/changing-text-of-radio-button
            // Where it was explained that the text of the radio button was now explicitly associated with the
            // use of <label>
            for (var i = 0; i < query.choices.length; i++)
                $("#answers").append("<div class='answerlist'><input type='radio' name='answers' class='ans' id='radio" + i + "' value='answer" + i
                    + "'><label for='radio" + i + "'>" + query.choices[i] + "</label></div>");
            if (answers.length > number)
                $("#radio" + answers[number]).prop("checked", true);
            if (number > 0)
                $("#back").prop("disabled", false);
        });
        $("#content").fadeIn(500);
    }

    function checkAnswer() {
        // Make sure a radio button is checked before proceeding. If one is checked add it to answers, else if
        // the last radio button is reached and it is not checked alert the user that they must select an answer.
        for (var i = 0; i < $("input").length; i++) {
            if ($("#radio" + i).is(":checked")) {
                answers[number] = i;
                break;
            }
            else if (i === $("input").length - 1 && !$("#radio" + i).is(":checked")) {
                $("#next").after("<p id='warning'>Please select an answer and then click next</p>");
                return false;
            }
        }

        // Check to see if the current radio button checked is the correct answer. If correct increment the
        // score 10 points. This answer helped figure out if a radio box was checked and allowed you to use it in
        // an if statement http://stackoverflow.com/a/12932116
        var query = allQuestions[number];
        if ($("#radio" + query.correctAnswer).is(":checked"))
            updateScore(10);
        number += 1;
        return true;
    }

    function finalScore() {
        $("#score").text("Final Score: " + total + "/" + totalQuestions * 10).show(1000);
        $("#question, #answers, #questCount, #next, #back").hide(10);
        $("#startagain").hide(100);
        if (total > 60)
            $("#result").show(1000);
        if (total < 70)
            $("#resultbad").show(1000);


    }

    function updateScore(change) {
        total += change;
        $("#score").text("Score: " + total);
    }

    $("#back").hide();
    $("#next").hide();
    $("#startagain").hide();
    $("#score").hide();
    $("#bar10").hide();
    $("#result").hide();
    $("#resultbad").hide();
    $("#div1").hide();
    $(".prog-bar").hide();

    $("#start").on('click', function () {
        $("#start").hide();
        $("#div1").show();
        $(".prog-bar").show();
        $('#h4Start').hide(1000);
        $("#next").show(1000);
        $("#bar").width('5%');
        newQuestionAnswers();
        updateScore(0);
    });

    $("#startagain").on('click', function () {

        location.reload();



    });

    $("#next").on('click', function () {
        $("#back").show(100);
        $("#warning").remove();
        if (checkAnswer()) {
            if (number < totalQuestions)
                newQuestionAnswers();
            else
                finalScore();


        }



        // Enable the back button if past first question
        if (number > 0)
            $("#back").prop("disabled", false);
        $("#bar").width('10%');

        if (number > 1)
            $("#bar").width('20%');
        if (number > 2)
            $("#bar").width('30%');
        if (number > 3)
            $("#bar").width('40%');
        if (number > 4)
            $("#bar").width('50%');
        if (number > 5)
            $("#bar").width('60%');
        if (number > 6)
            $("#bar").width('70%');
        if (number > 7)
            $("#bar").width('80%');
        if (number > 8)
            $("#bar").width('90%');
        if (number > 9)
            $("#bar").width('100%');



    });





    $("#back").on('click', function () {
        if (number === totalQuestions) {
            $("#score").hide();
            $("#question, #answers, #questCount, #next, #score").show(2500);
        }

        if (number > 0)
            $("#bar").width('5%');
        if (number > 1)
            $("#bar").width('10%');
        if (number > 2)
            $("#bar").width('20%');
        if (number > 3)
            $("#bar").width('30%');
        if (number > 4)
            $("#bar").width('40%');
        if (number > 5)
            $("#bar").width('50%');
        if (number > 6)
            $("#bar").width('60%');
        if (number > 7)
            $("#bar").width('70%');
        if (number > 8)
            $("#bar").width('80%');
        if (number > 9)
            $("#bar").width('90%');

        number -= 1;
        $("#back").prop("disabled", true);
        if (allQuestions[number].correctAnswer === answers[number])
            updateScore(-10);
        newQuestionAnswers();


    });
});



